(function () {
    "use strict";
    var module = angular.module("libranzaApp");
    const urlLibranzas = "http://localhost:3045/v1/company/"

    var fetchCompanies = function ($http) {
        var req = {
            method: 'GET',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        });
    }

    var companyUpdate = function ($http, company) {
        var req = {
            method: 'POST',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data: company
        }

        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var companyDelete = function ($http, nit) {
        var req = {
            method: 'DELETE',
            url: urlLibranzas + nit,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }

        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var controller = function ($http) {
        var model = this;
        model.companies = [];
        model.company = {
            nit: "",
            name: "",
            address: "",
            phones: "",
        };

        model.$onInit = function () {
            fetchCompanies($http).then(function (companies) {
                model.companies = companies;
            });
        }

        model.editCompany = function (company) {
            model.company = company;
        }

        model.updateCompany = function () {
            companyUpdate($http, this.company).then(function () {
                fetchCompanies($http).then(function (companies) {
                    model.companies = companies;
                });
                model.company = {
                    nit: "",
                    name: "",
                    address: "",
                    phones: "",
                };
            });

        }

        model.removeCompany = function (nit) {
            companyDelete($http, nit).then(function () {
                fetchCompanies($http).then(function (companies) {
                    model.companies = companies;
                });
            });

        }
    }

    module.component("companyList", {
        templateUrl: "libranzas-companies/company-list.component.html",
        controllerAs: "model",
        controller: ['$http', controller]
    })

})()