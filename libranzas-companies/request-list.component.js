(function () {
    "use strict";
    var module = angular.module("libranzaApp");
    const urlLibranzas = "http://localhost:3045/v1/request"

    //get all requests
    var fetchRequests = function ($http) {
        var req = {
            method: 'GET',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        });
    }

    var getRequestByCompany = function ($http, nit) {
        var req = {
            method: 'GET',
            url: urlLibranzas + "?companyId="+nit,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        });
    }

    var validateRequest = function ($http, model) {
        var req = {
            method: 'POST',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data : {
                "_id": model._id,
                "financialId": model.financialId,
                "employedId": model.employedId,
                "companyId": model.companyId,
                "ammount": model.ammount,
                "price": model.price,
                "validatedByFinancial": model.validatedByFinancial,
                "validatedByCompany": model.validatedByCompany,
                "dateRequest": model.dateRequest
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }


    var controller = function ($http) {
        var model = this;
        model.requests = [];
        model.request = {
            _id: "",
            dateRequest: "",
            ownerId: "",
            financialId: "",
            employedId: "",
            ammount: "",
            price: "",
            companyId: "",
            salesvalue: ""
        };

        model.getRequestByCompany = function () {
            getRequestByCompany($http, model.request.companyId).then(function (res) {
                model.requests = res.filter(function (item) {
                    return !item.validatedByCompany;
                });
            });
        }

        model.validate = function (currentModel) {
            currentModel.validatedByCompany = true;
            validateRequest($http, currentModel).then(function (res) {
                console.log(res);
                model.getRequestByCompany();
            });
        }

    }

    module.component("requestsList", {
        templateUrl: "libranzas-companies/request-list.component.html",
        controllerAs: "model",
        controller: ['$http', controller]
    })

})()