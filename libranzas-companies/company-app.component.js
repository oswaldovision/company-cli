(function(){
    "use strict";

    var module = angular.module("libranzaApp");

    module.component("companyApp",{
        templateUrl : "/libranzas-companies/company-app.component.html",
        $routeConfig : [
            { path : "/list" , component : "companyList" , name :"List"},
            { path : "/about" , component : "appAbout", name : "About"},
            { path : "/new" , component : "companyNew" , name :"New"},
            { path : "/requests" , component : "requestsList" , name :"ListRequests"},
            { path : "/**" , redirectTo : ["List"] }
        ]
    });

})()     