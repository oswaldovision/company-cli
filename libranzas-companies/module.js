(function () {
    "use strict";

    var module = angular.module("libranzaApp", ["ngComponentRouter", "angular-loading-bar"]);

    module.value("$routerRootComponent", "companyApp");

    module.component("appAbout", {
        templateUrl : "libranzas-companies/company-about.component.html"
    });
}());