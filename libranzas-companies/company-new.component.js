(function () {
    "use strict";
    var module = angular.module("libranzaApp");
    const urlLibranzas = "http://localhost:3045/v1/company"

    var addCompany = function ($http, model) {
        var req = {
            method: 'PUT',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data: {
                nit: model.nit,
                name: model.name,
                address: model.address,
                phones: model.phones
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var controller = function ($http, $timeout, $location) {
        var model = this;
        model.nit = "";
        model.name = "";
        model.address = "";
        model.phones = "";
        model.message = "";
        model.showError = false;
        model.doFade = false;

        model.create = function () {
            model.phones = model.phones.split(',');
            addCompany($http, model).then(function (result) {
                console.log(result);
                //reset
                model.showError = false;
                model.doFade = false;

                model.showError = true;
                model.message = result;

                $timeout(function () {
                    model.doFade = true;
                    $location.path( "/list" );
                }, 2500);

            }).catch(function (error) {
                model.message = error;
            });
        }
    }

    module.component("companyNew", {
        templateUrl: "libranzas-companies/company-new.component.html",
        controllerAs: "model",
        controller: ['$http', '$timeout', '$location', controller]
    })

})()